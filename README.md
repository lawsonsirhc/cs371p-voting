# CS371p: Object-Oriented Programming Collatz Repo

* Name: Meifeng Lin, Christopher Lawson

* EID: ml48527, cal4555

* GitLab ID: mei-f-lin, lawsonsirhc

* HackerRank ID: mei_f_lin

* Git SHA: 
bc7dfb6b0c6cf8d6a09c6ec258006d532c4e067a

* GitLab Pipelines: https://gitlab.com/lawsonsirhc/cs371p-voting/-/pipelines

* Estimated completion time: 30 hours

* Actual completion time: 15 hours

* Comments: None
