#ifndef Voting_hpp
#define Voting_hpp
#define DEBUG 0 // Macro to easily turn on and off debug statements

#include <vector>
#include <string>
#include <iostream>
#include <cassert>

using namespace std;

class Ballot {
public:

    // Contains the internal vote of this ballot
    vector<uint32_t> b;

    // Used to return the next vote to look at
    uint32_t nextValidVote;

    // Ballot constructed with a given vector of integers representing the candidates
    Ballot(vector<uint32_t> cur_b) : b(cur_b), nextValidVote(0) {}

    // Will return the vote while incrementing the nextValidVote variable
    uint32_t getVote() {
        return b[nextValidVote++];
    }

};


class Candidate {
public:
    // Store the name of the Candidate
    string name;

    // Store all the ballots that are currently voting for
    // this candidate
    vector<Ballot> ballots;

    // Candidate constructed with a name
    Candidate(string n) : name(n) {}

    // Add a new ballot to this candidates ballots
    void addBallot(Ballot b) {
        ballots.push_back(b);
    }

    // Will return all the ballots of this candidate
    // Clears the ballots before it returns
    vector<Ballot> giveUp() {
        vector<Ballot> temp = ballots;
        ballots.clear();
        return temp;
    }

    // Gets the votes by finding the size of the ballots vector
    uint32_t numVotes() {
        return ballots.size();
    }
};

void haveTie(vector<Candidate>& c, vector<Candidate*>& losers, vector<bool>& validC, bool& b);
string constructWinners(vector<Candidate> c);
string voting (vector<Candidate> c, vector<Ballot> b);


#endif