// ---------------
// TestVoting.cpp
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Voting.hpp"

using namespace std;

// ----------------
// voting
// ----------------

TEST(voting, voting_0) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("John Doe"));
    c.push_back(Candidate("Jane Smith"));
    c.push_back(Candidate("Sirhan Sirhan"));
    vector<uint32_t> ballot = {1, 2, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ballot = {2, 1, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ballot = {2, 3, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ballot = {1, 2, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ballot = {3, 1, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "John Doe");
}

TEST(voting, voting_1) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));

    vector<uint32_t> ballot = {2, 1, 4, 6, 7, 5, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 4, 5, 7, 2, 6, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 7, 4, 2, 1, 6, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {7, 2, 4, 3, 5, 6, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-2\nCandidate-3\nCandidate-5\nCandidate-7");
}

TEST(voting, voting_2) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));
    c.push_back(Candidate("Candidate-11"));
    c.push_back(Candidate("Candidate-12"));
    c.push_back(Candidate("Candidate-13"));
    c.push_back(Candidate("Candidate-14"));

    vector<uint32_t> ballot = {14, 11, 8, 10, 9, 5, 2, 6, 7, 1, 12, 3, 4, 13};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {11, 9, 5, 13, 6, 3, 4, 12, 8, 14, 1, 2, 10, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {14, 7, 10, 3, 9, 6, 13, 4, 2, 12, 8, 5, 11, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {14, 10, 12, 11, 9, 7, 2, 13, 5, 8, 6, 1, 4, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 8, 13, 4, 9, 1, 7, 10, 14, 2, 11, 12, 3, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 12, 2, 10, 4, 11, 3, 9, 5, 7, 6, 14, 13, 8};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 7, 12, 14, 2, 5, 13, 9, 11, 3, 10, 8, 4, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {11, 1, 14, 2, 4, 9, 6, 8, 3, 10, 13, 12, 5, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 12, 11, 5, 13, 10, 7, 4, 3, 2, 6, 8, 9, 14};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 7, 9, 2, 10, 1, 4, 14, 6, 13, 3, 11, 12, 8};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 2, 5, 7, 12, 3, 9, 14, 8, 4, 10, 13, 11, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {12, 5, 6, 13, 2, 4, 1, 14, 3, 8, 10, 11, 7, 9};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {12, 11, 13, 4, 9, 5, 8,1, 2, 6, 3, 10, 7, 14};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {13, 14, 1, 10, 7, 4, 6, 3, 2, 12, 11, 8, 5, 9};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 12, 7, 11, 10, 6, 9, 14, 3, 13, 5, 2, 4, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-1");
}

TEST(voting, voting_3) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));
    c.push_back(Candidate("Candidate-11"));
    c.push_back(Candidate("Candidate-12"));
    c.push_back(Candidate("Candidate-13"));
    c.push_back(Candidate("Candidate-14"));
    c.push_back(Candidate("Candidate-15"));
    c.push_back(Candidate("Candidate-16"));
    c.push_back(Candidate("Candidate-17"));

    vector<uint32_t> ballot = {14, 7, 16, 12, 5, 15, 10, 17, 9, 6, 3, 8, 11, 4, 2, 13, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {16, 2, 17, 12, 10, 7, 4, 15, 3, 9, 6, 11, 8, 1, 14, 13, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 7, 9, 14, 10, 17, 8, 5, 16, 4, 1, 12, 3, 13, 15, 11, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {14, 13, 12, 2, 16, 10, 7, 17, 3, 15, 8, 11, 1, 5, 4, 6, 9};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {12, 9, 4, 16, 6, 14, 11, 7, 8, 5, 2, 13, 1, 17, 10, 3, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {10, 16, 14, 12, 5, 17, 11, 8, 13, 6, 9, 3, 2, 4, 7, 15, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {16, 5, 7, 11, 14, 1, 4, 13, 3, 12, 8, 10, 9, 17, 15, 2, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {16, 11, 8, 17, 3, 6, 12, 10, 1, 9, 4, 5, 15, 13, 7, 2, 14};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {11, 7, 6, 8, 5, 4, 16, 3, 17, 2, 9, 12, 10, 14, 13, 1, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {12, 4, 15, 14, 1, 16, 9, 5, 7, 11, 6, 2, 13, 3, 17, 10, 8};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 14, 7, 17, 5, 13, 10, 12, 16, 4, 11, 8, 15, 2, 3, 6, 9};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 7, 1, 4, 9, 14, 2, 16, 13, 10, 12, 8, 17, 11, 15, 3, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {17, 3, 5, 4, 9, 10, 14, 15, 1, 7, 11, 2, 6, 16, 12, 8, 13};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {12, 14, 5, 6, 10, 13, 3, 16, 8, 17, 11, 7, 4, 9, 1, 2, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {11, 10, 6, 1, 7, 13, 4, 5, 16, 8, 15, 12, 3, 14, 17, 9, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {16, 10, 6, 4, 12, 14, 2, 1, 3, 15, 5, 17, 7, 11, 9, 8, 13};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 13, 16, 7, 8, 14, 9, 6, 12, 11, 15, 5, 10, 1, 17, 2, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 8, 10, 1, 4, 12, 16, 3, 11, 17, 13, 9, 7, 14, 2, 6, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-16");
}

TEST(voting, voting_4) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));

    vector<uint32_t> ballot = {1, 3, 2, 4, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 2, 3, 5, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 3, 2, 1, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {2, 4, 1, 3, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 2, 5, 3, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {4, 5, 3, 1, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 3, 2, 4, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 2, 5, 1, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {4, 3, 5, 2, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 3, 4, 1, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 3, 4, 5, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-1");
}

TEST(voting, voting_5) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));
    c.push_back(Candidate("Candidate-11"));
    c.push_back(Candidate("Candidate-12"));
    c.push_back(Candidate("Candidate-13"));

    vector<uint32_t> ballot = {10, 8, 2, 13, 9, 7, 3, 6, 1, 4, 12, 5, 11};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {9, 1, 4, 12, 8, 10, 7, 2, 13, 3, 11, 6, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 2, 6, 9, 8, 10, 11, 13, 5, 7, 1, 12, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {11, 1, 8, 6, 13, 7, 2, 3, 9, 12, 5, 10, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 7, 9, 5, 11, 3, 8, 4, 13, 10, 12, 2, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 4, 7, 2, 11, 3, 1, 12, 10, 5, 9, 8, 13};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 8, 9, 11, 7, 13, 4, 10, 1, 2, 6, 12, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 9, 7, 13, 2, 6, 4, 10, 11, 8, 1, 3, 12};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {9, 11, 6, 13, 10, 12, 8, 1, 4, 2, 5, 3, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {11, 13, 8, 6, 1, 4, 10, 9, 2, 7, 3, 12, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 9, 5, 6, 8, 11, 2, 3, 4, 7, 13, 10, 12};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {13, 4, 12, 10, 11, 8, 3, 2, 9, 1, 6, 5, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 11, 5, 12, 8, 3, 9, 4, 10, 6, 7, 2, 13};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {9, 3, 1, 8, 13, 7, 4, 11, 12, 10, 5, 6, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {4, 2, 12, 7, 6, 9, 10, 8, 1, 3, 13, 11, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 13, 11, 3, 2, 10, 1, 5, 4, 6, 9, 7, 12};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 7, 4, 1, 5, 13, 11, 9, 2, 12, 8, 10, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-9");
}

TEST(voting, voting_6) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));
    c.push_back(Candidate("Candidate-11"));
    c.push_back(Candidate("Candidate-12"));
    c.push_back(Candidate("Candidate-13"));
    c.push_back(Candidate("Candidate-14"));
    c.push_back(Candidate("Candidate-15"));
    c.push_back(Candidate("Candidate-16"));
    c.push_back(Candidate("Candidate-17"));
    c.push_back(Candidate("Candidate-18"));
    c.push_back(Candidate("Candidate-19"));
    c.push_back(Candidate("Candidate-20"));

    vector<uint32_t> ballot = {7, 11, 20, 13, 10, 18, 5, 17, 2, 6, 9, 16, 8, 19, 15, 3, 1, 12, 4, 14};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {20, 4, 1, 16, 13, 12, 3, 11, 10, 14, 15, 5, 7, 18, 9, 19, 6, 2, 8, 17};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {19, 17, 1, 18, 11, 4, 6, 16, 8, 5, 3, 2, 10, 20, 9, 7, 14, 12, 13, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {14, 16, 3, 8, 2, 15, 13, 9, 17, 12, 6, 20, 1, 7, 19, 4, 11, 5, 10, 18};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {7, 20, 4, 9, 8, 19, 11, 12, 10, 14, 18, 1, 6, 3, 5, 2, 13, 17, 16, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {7, 4, 16, 12, 20, 6, 18, 11, 2, 9, 14, 3, 5, 13, 17, 8, 10, 19, 1, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 12, 7, 18, 15, 20, 9, 5, 6, 3, 17, 1, 2, 13, 4, 14, 11, 19, 10, 16};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {17, 3, 11, 14, 15, 10, 12, 16, 5, 20, 6, 13, 2, 19, 4, 7, 9, 18, 8, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {2, 18, 7, 17, 14, 3, 13, 10, 15, 1, 4, 11, 6, 12, 8, 9, 16, 19, 5, 20};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {10, 9, 6, 4, 3, 8, 2, 5, 14, 12, 7, 1, 13, 11, 15, 18, 19, 17, 16, 20};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {14, 7, 5, 11, 18, 13, 8, 6, 4, 6, 12, 3, 2, 10, 1, 15, 20, 16, 17, 19};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-7");
}

TEST(voting, voting_7) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));

    vector<uint32_t> ballot = {7, 10, 2, 8, 9, 3, 1, 5, 6, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 2, 1, 9, 5, 10, 6, 3, 4, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 1, 8, 10, 2, 5, 9, 3, 4, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {2, 3, 1, 10, 5, 8, 6, 4, 9, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 7, 3, 8, 9, 2, 6, 1, 10, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {7, 8, 3, 10, 5, 1, 2, 9, 6, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 10, 7, 5, 9, 2, 8, 3, 4, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {2, 8, 1, 6, 3, 9, 4, 10, 5, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 9, 8, 7, 6, 1, 2, 4, 10, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {4, 5, 1, 9, 10, 8, 2, 7, 6, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 2, 5, 1, 3, 7, 6, 4, 10, 9};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 7, 2, 1, 3, 5, 4, 8, 10, 9};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {7, 3, 9, 10, 5, 8, 1, 4, 2, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-5");
}

TEST(voting, voting_8) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));

    vector<uint32_t> ballot = {2, 1, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {2, 1, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 2, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 2, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 3, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 1, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 3, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 2, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {2, 3, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 1, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 2, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-1");
}

TEST(voting, voting_9) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));

    vector<uint32_t> ballot = {1, 3, 4, 2, 10, 7, 5, 8, 9, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 2, 1, 4, 3, 8, 9, 6, 7, 10};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 4, 9, 2, 6, 5, 1, 3, 7, 10};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 3, 4, 1, 7, 2, 9, 10, 5, 6};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {5, 1, 7, 3, 8, 6, 2, 10, 9, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {4, 2, 3, 9, 1, 6, 8, 5, 7, 10};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 8, 4, 9, 6, 1, 5, 7, 2, 10};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 9, 4, 5, 7, 10, 6, 1, 2, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 5, 4, 7, 2, 1, 10, 9, 6, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 10, 2, 7, 3, 6, 4, 5, 9, 1};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 4, 2, 5, 10, 1, 3, 9, 6, 7};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 5, 3, 7, 10, 1, 4, 9, 6, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 3, 2, 1, 10, 7, 5, 9, 8, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 5, 9, 4, 3, 10, 8, 1, 7, 2};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {6, 2, 8, 1, 10, 3, 7, 4, 9, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 9, 7, 6, 10, 1, 4, 2, 5, 8};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-3\nCandidate-8");
}

TEST(voting, voting_10) {
    vector<Candidate> c;
    vector<Ballot> b;
    c.push_back(Candidate("Candidate-1"));
    c.push_back(Candidate("Candidate-2"));
    c.push_back(Candidate("Candidate-3"));
    c.push_back(Candidate("Candidate-4"));
    c.push_back(Candidate("Candidate-5"));
    c.push_back(Candidate("Candidate-6"));
    c.push_back(Candidate("Candidate-7"));
    c.push_back(Candidate("Candidate-8"));
    c.push_back(Candidate("Candidate-9"));
    c.push_back(Candidate("Candidate-10"));
    c.push_back(Candidate("Candidate-11"));
    c.push_back(Candidate("Candidate-12"));
    c.push_back(Candidate("Candidate-13"));
    c.push_back(Candidate("Candidate-14"));
    c.push_back(Candidate("Candidate-15"));
    c.push_back(Candidate("Candidate-16"));

    vector<uint32_t> ballot = {7, 16, 10, 12, 5, 8, 11, 9, 13, 4, 2, 6, 15, 1, 3, 14};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {15, 2, 10, 9, 3, 11, 1, 6, 13, 14, 4, 16, 7, 5, 12, 8};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {15, 10, 14, 16, 5, 2, 7, 13, 9, 11, 6, 8, 3, 12, 1, 4};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {15, 10, 9, 3, 14, 4, 2, 16, 6, 1, 7, 11, 8, 12, 13, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {15, 10, 14, 5, 6, 2, 4, 9, 3, 13, 16, 1, 7, 12, 8, 11};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {1, 12, 14, 6, 4, 5, 13, 8, 9, 10, 7, 11, 3, 16, 2, 15};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 4, 9, 13, 16, 11, 2, 5, 7, 14, 10, 1, 6, 15, 3, 12};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {3, 14, 6, 15, 2, 9, 7, 1, 8, 12, 16, 11, 10, 4, 13, 5};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {15, 9, 2, 8, 14, 11, 13, 5, 16, 7, 10, 12, 6, 4, 1, 3};
    b.push_back(Ballot(ballot));
    ballot.clear();

    ballot = {8, 2, 13, 9, 5, 16, 4, 11, 6, 3, 1, 15, 7, 12, 10, 14};
    b.push_back(Ballot(ballot));
    ballot.clear();
    ASSERT_EQ(voting(c, b), "Candidate-15");
}

TEST(constructWinners, constructWinners_0) {
    vector<Candidate> c;
    vector<Ballot> b;
    Candidate c1{"Candidate-1"};
    vector<uint32_t> ballot = {1, 2, 3};
    c1.addBallot(Ballot(ballot));
    c.push_back(c1);
    Candidate c2{"Candidate-2"};
    c.push_back(c2);
    Candidate c3{"Candidate-3"};
    ballot = {3, 2, 1};
    c3.addBallot(Ballot(ballot));
    c.push_back(c3);
    ASSERT_EQ(constructWinners(c), "Candidate-1\nCandidate-3");
}

TEST(constructWinners, constructWinners_1) {
    vector<Candidate> c;
    vector<Ballot> b;
    Candidate c1{"Candidate-1"};
    vector<uint32_t> ballot = {1, 2, 3};
    c1.addBallot(Ballot(ballot));
    c.push_back(c1);
    Candidate c2{"Candidate-2"};
    ballot = {2, 1, 3};
    c2.addBallot(Ballot(ballot));
    c.push_back(c2);
    Candidate c3{"Candidate-3"};
    ballot = {3, 2, 1};
    c3.addBallot(Ballot(ballot));
    c.push_back(c3);
    ASSERT_EQ(constructWinners(c), "Candidate-1\nCandidate-2\nCandidate-3");
}

TEST(haveTie, haveTie_0) {
    vector<Candidate> c;
    vector<Ballot> b;
    Candidate c1("Candidate-1");
    vector<uint32_t> ballot = {1, 2, 3};
    c1.addBallot(Ballot(ballot));
    c.push_back(c1);
    Candidate c2("Candidate-2");
    c.push_back(c2);
    Candidate c3("Candidate-3");
    ballot = {3, 2, 1};
    c3.addBallot(Ballot(ballot));
    c.push_back(c3);
    vector<Candidate*> losers;
    vector<bool> validC(3, true);
    bool tie;
    haveTie(c, losers, validC, tie);
    ASSERT_EQ(tie, true);
    ASSERT_EQ(validC[0], false);
    ASSERT_EQ(validC[1], true);
    ASSERT_EQ(validC[2], false);
    ASSERT_EQ(losers[0]->name, "Candidate-1");
    ASSERT_EQ(losers[1]->name, "Candidate-3");
}

TEST(haveTie, haveTie_1) {
    vector<Candidate> c;
    vector<Ballot> b;
    Candidate c1("Candidate-1");
    vector<uint32_t> ballot = {1, 2, 3};
    c1.addBallot(Ballot(ballot));
    ballot = {1, 3, 2};
    c1.addBallot(Ballot(ballot));
    c.push_back(c1);
    Candidate c2("Candidate-2");
    c.push_back(c2);
    Candidate c3("Candidate-3");
    ballot = {3, 2, 1};
    c3.addBallot(Ballot(ballot));
    c.push_back(c3);
    vector<Candidate*> losers;
    vector<bool> validC = {true, false, true};
    bool tie;
    haveTie(c, losers, validC, tie);
    ASSERT_EQ(tie, false);
    ASSERT_EQ(validC[0], true);
    ASSERT_EQ(validC[1], false);
    ASSERT_EQ(validC[2], false);
    ASSERT_EQ(losers[0]->name, "Candidate-3");
}

TEST(ballot, getVote_0) {
    vector<uint32_t> ballot = {1, 2, 3};
    Ballot b(ballot);
    ASSERT_EQ(b.getVote(), 1);
    ASSERT_EQ(b.getVote(), 2);
    ASSERT_EQ(b.getVote(), 3);
}

TEST(ballot, getVote_1) {
    vector<uint32_t> ballot = {3, 2, 1};
    Ballot b(ballot);
    ASSERT_EQ(b.getVote(), 3);
    ASSERT_EQ(b.getVote(), 2);
    ASSERT_EQ(b.getVote(), 1);
}

TEST(candidate, addBallot_0) {
    Candidate c1("Candidate-1");
    vector<uint32_t> ballot = {1, 2, 3};
    ASSERT_EQ(c1.ballots.size(), 0);
    c1.addBallot(ballot);
    ASSERT_EQ(c1.ballots.size(), 1);
}

TEST(candidate, addBallot_1) {
    Candidate c1("Candidate-1");
    vector<uint32_t> ballot = {1, 2, 3};
    c1.ballots.push_back(ballot);
    ballot = {1, 3, 2};
    c1.ballots.push_back(ballot);

    ASSERT_EQ(c1.ballots.size(), 2);
    ballot = {1, 2, 3};
    c1.addBallot(ballot);
    ASSERT_EQ(c1.ballots.size(), 3);

    Ballot c1Ballot = c1.ballots.at(2);
    ASSERT_EQ(c1Ballot.b.size(), ballot.size());
    for(uint32_t i = 0; i < c1Ballot.b.size(); i++) {
        ASSERT_EQ(c1Ballot.b.at(i), ballot.at(i));
    }
}

TEST(candidate, giveUp_0) {
    Candidate c1("Candidate-1");

    vector<uint32_t> ballot = {1, 2, 3};
    c1.ballots.push_back(Ballot(ballot));

    vector<Ballot> returned = c1.giveUp();
    ASSERT_EQ(c1.ballots.size(), 0);
}

TEST(candidate, giveUp_1) {
    Candidate c1("Candidate-1");
    vector<Ballot> outside;

    vector<uint32_t> ballot = {1, 2, 3};
    Ballot temp = Ballot(ballot);
    c1.ballots.push_back(temp);
    outside.push_back(temp);

    ballot = {1, 3, 2};
    temp = Ballot(ballot);
    c1.ballots.push_back(temp);
    outside.push_back(temp);

    vector<Ballot> returned = c1.giveUp();

    ASSERT_EQ(returned.size(), outside.size());
    for(uint32_t i = 0; i < returned.size(); i++) {
        Ballot returnedCheck = returned.at(i);
        Ballot outsideCheck = outside.at(i);
        ASSERT_EQ(returnedCheck.b.size(), outsideCheck.b.size());
        for(uint32_t j = 0; j < returnedCheck.b.size(); j++) {
            ASSERT_EQ(returnedCheck.b.at(j), outsideCheck.b.at(j));
        }
    }
}

TEST(candidate, numVotes_0) {
    Candidate c1("Candidate-1");

    vector<uint32_t> ballot = {1, 2, 3};
    c1.ballots.push_back(Ballot(ballot));

    ASSERT_EQ(c1.numVotes(), 1);
}

TEST(candidate, numVotes_1) {
    Candidate c1("Candidate-1");

    vector<uint32_t> ballot = {1, 2, 3};
    c1.ballots.push_back(Ballot(ballot));

    ballot = {1, 2, 3};
    c1.ballots.push_back(Ballot(ballot));

    ballot = {1, 2, 3};
    c1.ballots.push_back(Ballot(ballot));

    ASSERT_EQ(c1.numVotes(), 3);
}
