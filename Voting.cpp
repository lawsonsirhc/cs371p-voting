#include "Voting.hpp"
#include <climits>

/*
    This file will contain the voting() method, when given a vector of Candidates
    and a vector of Ballots, it will return a string that contains the winners.

    @file Voting.cpp
*/


/*
    Method will determine whether there is a tie or not. If there is a tie, then
    the method will place the candidates tied for last into losers. It will also mark
    those candidates that lost in the validC array.

    @param c      - A vector of all candidate objects
    @param losers - A vector that, by the end of execution, will contain the
                    candidate objects that lost
    @param validC - A vector of bools that represents which Candidates are still in the running
    @param b      - The bool that represents whether there is a tie or not.
*/
void haveTie(vector<Candidate>& c, vector<Candidate*>& losers, vector<bool>& validC, bool& b) {
    if(DEBUG) {
        cout << "have tie" << endl;
        for(uint32_t i = 0; i < validC.size(); i++) {
            cout << validC[i] << " ";
        }
        cout << endl;
    }

    /*
        Will have a running current minimum to determine which candidate(s) are tied
        for last. Keep in mind that candidates that have 0 votes should not be considered

        Because there can be the case that there are multiple minimums, there are 4 cases that need
        to be considered.

        1) If the current min has not been set, and the current candidate's votes is not 0
          - Here we set the current minimum to the current candidate's number of votes
            and add the current candidate to losers (make sure that it's the reference to candidate).
            Lastly, mark the current candidate as no longer running.

        2) If the current min has been set, the current candidate's votes is not 0, and
           the current candidate's votes is less than the current min
          - Here we need to reset the current min to the new candidates number of votes.
            This means that anyone that was in the loser vector needs to be emptied, and the valid candidates
            needs to be reset. Then we add the new candidate gets put in the loser vector and marked
            as not in the running.

        3) If the current candidate has the same number of votes as the current minimum and the current minimum
           has been set.
          - Add current candidate to the loser vector and mark the candidate as no longer running.

        4) Lastly, if the current candidate has more votes than the current minimum.
          - This just means that we can't have a tie as there are two candidates with different number of votes that are
            not 0
    */

    uint32_t curMin = UINT32_MAX;
    bool tie = true;

    // Save a previous copy of the valid candidates to reset to
    vector<bool> reset = validC;

    for(uint32_t i = 0; i < c.size(); i++) {
        if(curMin == UINT32_MAX && c[i].numVotes() != 0) {
            curMin = c[i].numVotes();
            losers.push_back(&c[i]);
            validC[i] = false; // Mark as no longer running

        } else if(curMin != UINT32_MAX && c[i].numVotes() != 0 && curMin > c[i].numVotes()) {
            curMin = c[i].numVotes();
            tie = false;

            //clear losers add new losers
            losers.clear();
            validC = reset;
            losers.push_back(&c[i]);
            validC[i] = false; // Mark as no longer running

        } else if(curMin != UINT32_MAX && curMin == c[i].numVotes()) { //found tie
            losers.push_back(&c[i]);
            validC[i] = false; // Mark as no longer running
        } else if(c[i].numVotes() != 0) { //found greater number of votes
            tie = false;
        }
    }

    // By the end, we know if we tripped the "replace min" or "greater number" cases,
    // we set tie to false. Other wise b will be set to true.
    b = tie;

    if(DEBUG) {
        cout << "end of have tie " << endl;
        for(uint32_t i = 0; i < validC.size(); i++) {
            cout << validC[i] << " ";
        }
        cout << endl;
    }
}

/*
    @param c - A vector of Candidates, assumes that all loser candidates are 0 and
               all tied candidates have the same number of votes.
    @return - A string of all tied winners in order of c, separated by new lines (\n).
              So that when printed, it will print out correctly. No new line on the
              last candidate.
*/
string constructWinners(vector<Candidate> c) {
    string winners = "";
    for(uint32_t i = 0; i < c.size(); i++) {
        // Assumes that losers have no votes
        // Assumes that all ties for winners have the same non-zero value
        if(c[i].numVotes() != 0 && winners.length() == 0) {
            winners = winners + c[i].name;
        } else if(c[i].numVotes() != 0) {
            winners = winners + "\n" + c[i].name;
        }
    }
    return winners;
}

/*
    @param c - A vector of Candidates
    @param b - A vector of Ballots
    @return - a string of winning candidates. If there are multiple winners,
              they will be delimited by new line (\n) characters (except on the last winner).
*/
string voting(vector<Candidate> c, vector<Ballot> b) {
    assert(c.size() != 0);
    assert(b.size() != 0);

    assert(c.size() <= 20);
    assert(b.size() <= 1000);
    if(DEBUG) {
        cout << "Printing Candidates" << endl;
        for(uint32_t i = 0; i < c.size(); i++) {
            cout << "Name: " << c[i].name;
            cout << " Address: " << &c[i];
            cout << endl;
        }
        cout << endl;
    }

    uint32_t majority = (b.size() / 2) + 1;

    // This vector will represent whether a particular candidate at index i
    // is in the running or not.
    // A candidate is marked as no longer running when they have tied for last
    // in the haveTie(...) method.
    vector<bool> validC(c.size(), true);

    // Will contain the valid (untallied) votes at a particular time of counting votes
    // Initially all votes passed in are valid.
    vector<Ballot> validBallots = b;

    while(true) {

        // Loop to tally all votes in valid Ballots
        for(uint32_t i = 0; i < validBallots.size(); i++) {
            Ballot curBallot = validBallots[i];

            // Get the next valid vote on a particular ballot
            uint32_t vote = curBallot.getVote();

            // Because a ballot might have multiple candidates on their ballot
            // that are marked as "no longer running", we need to keep looking until
            // the vote is for someone that is in the running.

            // Indexing at vote - 1 as candidates are 0 indexable while they are
            // stored internally with 1 indexable
            while(validC[vote - 1] != true) {
                vote = curBallot.getVote();
            }

            if(DEBUG) {
                cout << "ADD BALLOT CALLED ON CANDIDATE: " << &c[vote - 1] << endl;
            }

            // Will add the current ballot to a particular candidates ballots.
            c[vote - 1].addBallot(curBallot);

            // If the candidate that got the current ballot wins majority, return early
            if(c[vote - 1].numVotes() >= majority) {
                string winner = c[vote - 1].name;
                assert(winner.length() != 0);
                return winner;
            }
        }

        // If we are here then we need to now need to clear the valid ballots
        // and replace them with the ballots of the candidates that tied for last.

        if(DEBUG) {
            cout << endl;
            cout << "CUR VOTES" << endl;

            for(uint32_t i = 0; i < c.size(); i++) {
                cout << &c[i] << " " << c[i].numVotes() << endl;
            }
            cout << endl;
        }


        // First, we need to check for a flat tie between all candidates in the running
        bool tie;
        vector<Candidate*> losers; // Will contain the losers of this particular counting cycle

        if(DEBUG) {
            cout << "OUTSIDE LOSERS: " << &losers << endl;
            for(uint32_t i = 0; i < validC.size(); i++) {
                cout << validC[i] << " ";
            }
            cout << endl;
        }

        // After this method call:
        //  losers will contain the candidates that tied for last.
        //  validC will be updated to correctly represent running candidates
        //  tie will represent whether there is a tie or not.
        haveTie(c, losers, validC, tie);

        if(DEBUG) {
            cout << "outside of have tie" << endl;
            for(uint32_t i = 0; i < validC.size(); i++) {
                cout << validC[i] << " ";
            }
            cout << endl;
        }

        // If there is a flat tie, go through the process of getting the
        // winning tied candidates.
        if(tie) {
            string winners = constructWinners(c);
            assert(winners.length() != 0);
            return winners;
        }

        // Else, we now need to update valid ballots with the ballots of the
        // losers.
        vector<Ballot> tempValidBallots;

        for(uint32_t i = 0; i < losers.size(); i++) {
            if(DEBUG) {
                cout << "LOSER: " << losers[i]->name << " ADDRESS: " << losers[i] << endl;
                cout << "BEFORE GIVEUP: " << losers[i]->numVotes() << endl;
            }

            // Take the loserBallots from the current Looser
            // After giveUp is called, the candidates number of votes gets set to 0.
            vector<Ballot> loserBallots = losers[i]->giveUp();

            if(DEBUG) {
                cout << "AFTER GIVEUP: " << losers[i]->numVotes() << endl;
            }

            // And insert them into temporary storage.
            for(uint32_t j = 0; j < loserBallots.size(); j++) {
                Ballot curLoserBallot = loserBallots[j];
                tempValidBallots.push_back(curLoserBallot);
            }
        }

        // Reset valid Ballots with the temporary storage that is now filled
        // with the loser ballots.
        validBallots = tempValidBallots;

        if(DEBUG) {
            cout << "NEW VALID BALLOTS" << endl;
            for(uint32_t i = 0; i < validBallots.size(); i++) {
                Ballot tempBallot = validBallots[i];
                for(uint32_t j = 0; j < tempBallot.b.size(); j++) {
                    cout << tempBallot.b[j] << " ";
                }
                cout << endl;
            }
        }


        if(DEBUG) {
            cout << "ABOUT TO PRINT LAST" << endl;
            for(uint32_t i = 0; i < c.size(); i++) {
                cout << "Candidate: " << c[i].name << " NumVotes: " << c[i].numVotes() << endl;
            }
        }
    }
}