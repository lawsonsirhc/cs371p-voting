// --------------
// RunVoting.cpp
// --------------

// --------
// includes
// --------

#include <sstream>  // istringstream
#include <bits/stdc++.h>

#include "Voting.hpp"

using namespace std;
/*
Example input:

2 // number of test cases

3              // number of candidates
John Doe       // limited to 80 chars, up to 20 candidates
Jane Smith
Sirhan Sirhan
1 2 3          // ballots, up to 1,000
2 1 3          // this voter has Jane   as their first choice
2 3 1          // this voter has Jane   as their first choice
1 2 3          // this voter has John   as their first choice
3 1 2          // this voter has Sirhan as their first choice

3
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3
2 3 1
1 2 3
3 1 2
EOF
*/


// ----
// main
// ----

int main () {
    string s;
    while (getline(cin, s)) {
        // ----
        // read Num tests
        // ----

        istringstream iss(s);
        uint32_t numTests;
        iss >> numTests;

        assert(numTests > 0);
        assert(numTests <= 100);

        // Skip a line
        getline(cin, s);

        // ----
        // read in numTests number of tests

        for(uint32_t i = 0; i < numTests; i++) {
            // cout << "on test i: " << i << endl;
            // Read in numCandidates
            getline(cin, s);
            istringstream iss(s);
            uint32_t numCandi;
            iss >> numCandi;
            // cout << "candidates: " << numCandi << endl;
            vector<Candidate> allCandidates;
            vector<Ballot> allBallots;
            // Read in the candidates
            for(uint32_t j = 0; j < numCandi; j++) {
                getline(cin, s);
                // istringstream iss(s);
                // Right now just print them out
                // Will need to eventually do more with them
                string curCandi = s;
                // iss >> curCandi;
                // cout << curCandi << endl;

                Candidate tempCandi {curCandi};
                allCandidates.push_back(tempCandi);
            }

            // Read in the ballots for each person
            // idk how many, so while cin is not empty
            getline(cin, s);
            while(s.length() != 0 && cin) {
                istringstream iss(s);
                // cout << "string: " << s << " length: " << s.length() << endl;
                // For now I'm going to do a hard 3 candidates, but I know I will need to put into an array
                vector<uint32_t> cur_ballot;
                for(int num; iss >> num;) {
                    cur_ballot.push_back(num);
                }

                // cout << "Printing vector" << endl;
                // for(uint32_t i = 0; i < cur_ballot.size(); i++){
                //     cout << cur_ballot.at(i) << " ";
                // }

                // cout << endl;

                Ballot temp_ballot{cur_ballot};
                allBallots.push_back(temp_ballot);

                getline(cin, s);
            }

            // Now call voting to see who wins.
            assert(allCandidates.size() != 0);
            assert(allBallots.size() != 0);
            string winner = voting(allCandidates, allBallots);
            assert(winner.length() != 0);
            if(i != numTests - 1) {
                cout << winner;
                cout << endl;
                cout << endl;
            } else {
                cout << winner;
            }

        }
    }
    return 0;
}
