.DEFAULT_GOAL := all
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-11
    GTEST    := /usr/local/src/googletest-master
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
    VALGRIND :=
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/src/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
    VALGRIND := valgrind
else
    BOOST    := /usr/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-11
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
    VALGRIND := /lusr/opt/valgrind-3.17/bin/valgrind
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Voting.log:
	git log > Voting.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Voting code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Voting code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Voting.cpp
	git add Voting.hpp
	-git add Voting.log
	-git add html
	git add Makefile
	git add README.md
	git add RunVoting.cpp
	git add RunVoting.ctd
	git add TestVoting.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunVoting: Voting.hpp Voting.cpp RunVoting.cpp
	-$(CPPCHECK) Voting.cpp
	-$(CPPCHECK) RunVoting.cpp
	$(CXX) $(CXXFLAGS) Voting.cpp RunVoting.cpp -o RunVoting

# compile test harness
TestVoting: Voting.hpp Voting.cpp TestVoting.cpp
	-$(CPPCHECK) Voting.cpp
	-$(CPPCHECK) TestVoting.cpp
	$(CXX) $(CXXFLAGS) Voting.cpp TestVoting.cpp -o TestVoting $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                 \
    RunVoting                            \
    TestVoting

# compile all
all: $(FILES)

# execute test harness
test: TestVoting
	-$(VALGRIND) ./TestVoting
	$(GCOV) TestVoting-Voting.cpp | grep -B 2 "cpp.gcov"

# clone the Voting test repo
../cs371p-voting-tests:
	git clone https://gitlab.com/gpdowning/cs371p-voting-tests.git ../cs371p-voting-tests

# test files in the Voting test repo
T_FILES := `ls ../cs371p-voting-tests/*.in`

# check integrity of all the test files in the Voting test repo
ctd-check: ../cs371p-voting-tests
	-for v in $(T_FILES); do echo $(CHECKTESTDATA) RunVoting.ctd $$v; $(CHECKTESTDATA) RunVoting.ctd $$v; done

# execute the run harness against a test file in the Voting test repo and diff with the expected output
../cs371p-voting-tests/%: RunVoting
	$(CHECKTESTDATA) RunVoting.ctd $@.in
	./RunVoting < $@.in > $@.tmp
	diff $@.tmp $@.ans

# execute the run harness against all of the test files in the Voting test repo and diff with the expected output
run: ../cs371p-voting-tests
	-for v in $(T_FILES); do make $${v/.in/}; done


# auto format the code
format:
	$(ASTYLE) Voting.cpp
	$(ASTYLE) Voting.hpp
	$(ASTYLE) RunVoting.cpp
	$(ASTYLE) TestVoting.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=         \
    .gitignore     \
    .gitlab-ci.yml \
    Voting.log    \
    html

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.gen
	rm -f *.plist
	rm -f *.tmp
	rm -f RunVoting
	rm -f TestVoting
	rm -f ../cs371p-voting-tests/*.tmp

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Voting.log
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo  'shell uname -p'
	@echo $(shell uname -p)

	@echo
	@echo  'shell uname -s'
	@echo $(shell uname -s)

	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version

	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version

	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version

	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version

	@echo
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version

	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version

	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version

	@echo
	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version

	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version

ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif

	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version

	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	@echo "% grep \"set(GOOGLETEST_VERSION\" $(GTEST)/CMakeLists.txt"
	@grep "set(GOOGLETEST_VERSION" $(GTEST)/CMakeLists.txt
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a